/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Datacollector.Parsing
{
    /// <summary>
    /// Definition of frequently used character codes.
    /// </summary>
	public static class Chars
	{
        /// <summary>
        /// Sart byte.
        /// </summary>
		public const byte STX = 2;
        /// <summary>
        /// End byte.
        /// </summary>
		public const byte ETX = 3;
        /// <summary>
        /// Line feed.
        /// </summary>
		public const byte LF = 10;
        /// <summary>
        /// Carriage return.
        /// </summary>
		public const byte CR = 13;
        /// <summary>
        /// Device control 1.
        /// </summary>
		public const byte DC1 = 17;
        /// <summary>
        /// Device control 2.
        /// </summary>
		public const byte DC2 = 18;
        /// <summary>
        /// Device control 3.
        /// </summary>
		public const byte DC3 = 19;
        /// <summary>
        /// Device control 4.
        /// </summary>
		public const byte DC4 = 20;
        /// <summary>
        /// Negative Acknowledge.
        /// </summary>
		public const byte NAK = 21;
        /// <summary>
        /// Synchronous Idle.
        /// </summary>
		public const byte SYN = 22;
        /// <summary>
        /// End of transmission block.
        /// </summary>
		public const byte ETB = 23;
        /// <summary>
        /// Cancel.
        /// </summary>
		public const byte CAN = 24;
		
        /// <summary>
        /// Determines if the specified character is printable.
        /// </summary>
        /// <returns><c>true</c> if c is printable; otherwise, <c>false</c>.</returns>
        /// <param name="c">character.</param>
		public static bool IsPrintable(byte c) {
			return c >= ' ' && c <= '~';
		}
	}
}

