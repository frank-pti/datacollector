/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Datacollector.Transportation;
using Internationalization;
using ProbeNet.Messages.Base;

namespace Datacollector.Parsing
{
    /// <summary>
    /// Base class for parsers.
    /// </summary>
	public abstract class Parser
	{
		private Transport transport;

        /// <summary>
        /// Delegate method for parsing error event.
        /// </summary>
		public delegate void ParsingErrorCallback(string error);
        /// <summary>
        /// Occurs when parsing error.
        /// </summary>
		public event ParsingErrorCallback ParsingError;
		
        /// <summary>
        /// Delegate method for parsing data event.
        /// </summary>
		public delegate void ParsingDataCallback(Byte[] data);
        /// <summary>
        /// Occurs when parsing data.
        /// </summary>
		public event ParsingDataCallback ParsingData;

        /// <summary>
        /// Delegate method for sequence received event.
        /// </summary>
		public delegate void SequenceReceivedCallback(Sequence sequence, SequenceDescription description);
        /// <summary>
        /// Occurs when sequence received.
        /// </summary>
		public event SequenceReceivedCallback SequenceReceived;

        /// <summary>
        /// Delegate method for transport changed event.
        /// </summary>
		public delegate void TransportChangedDelegate();
        /// <summary>
        /// Occurs when transport changed.
        /// </summary>
		public event TransportChangedDelegate TransportChanged;

        /// <summary>
        /// Delegate method for license information received event.
        /// </summary>
		public delegate void LicenseInformationReceivedDelegate(string modelNumber, string serialNumber);
        /// <summary>
        /// Occurs when license information received.
        /// </summary>
		public event LicenseInformationReceivedDelegate LicenseInformationReceived;

        /// <summary>
        /// The i18n.
        /// </summary>
		protected I18n i18n;
		
        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Parsing.Parser"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="transport">Transport.</param>
		public Parser(I18n i18n, Transport transport)
		{
			this.i18n = i18n;
			Transport = transport;
		}

		private void HandleTransportDataReceived (Byte[] data)
		{
			NotifyParsingData(data);
			Parse(data, 0, data.Length);
		}

        /// <summary>
        /// Parse the specified data, beginning at offset with length count.
        /// </summary>
        /// <param name="data">Data to parse.</param>
        /// <param name="offset">Offset to start at.</param>
        /// <param name="count">Count.</param>
		public void Parse(byte[] data, int offset, int count)
		{
			if (data == null) {
				throw new ArgumentNullException();
			}
			if (offset + count > data.Length) {
				throw new ArgumentOutOfRangeException("The offset plus the count must not be longer than the data");
			}
			if (count == 0) {
				return;
			}
			if (count < 0 || offset < 0) {
				throw new ArgumentOutOfRangeException("Offset and count must not be smaller than 0");
			}
			ParseCheckedData(data, offset, count);
		}

        /// <summary>
        /// Parses data where the offset and count parameters have been checked to contain meaninful values
        /// (offset and count describe indices within the data array).
        /// </summary>
        /// <param name="data">Data to parse.</param>
        /// <param name="offset">Offset to start at.</param>
        /// <param name="count">Count.</param>
		public abstract void ParseCheckedData(byte[] data, int offset, int count);

        /// <summary>
        /// Notifies the parsing error.
        /// </summary>
        /// <param name="error">Error message.</param>
        /// <param name="arguments">Error message arguments.</param>
		public void NotifyParsingError(string error, params object[] arguments)
		{
			NotifyParsingError(String.Format(error, arguments));
		}
		
        /// <summary>
        /// Notifies the parsing error.
        /// </summary>
        /// <param name="error">Error message.</param>
		public void NotifyParsingError(string error)
		{
			if (ParsingError != null) {
				ParsingError(error);
			}
		}

        /// <summary>
        /// Notifies the sequence received.
        /// </summary>
        /// <param name="sequence">Sequence that has been received.</param>
        /// <param name="description">The sequence description.</param>
		protected void NotifySequenceReceived(Sequence sequence, SequenceDescription description)
		{
			if (SequenceReceived != null) {
				SequenceReceived(sequence, description);
			}
		}

        /// <summary>
        /// Gets a value indicating whether the configured model number matches the model number transmitted by the
         /// device.
        /// </summary>
        /// <value><c>true</c> if model number is verified; otherwise, <c>false</c>.</value>
		public abstract bool VerifyLicenseModelNumber {
			get;
		}

        /// <summary>
        /// Gets a value indicating whether the configured serial number matches the serial number transmitted by the
        /// device.
        /// </summary>
        /// <value><c>true</c> if serial number is verified; otherwise, <c>false</c>.</value>
		public abstract bool VerifyLicenseSerialNumber {
			get;
		}

        /// <summary>
        /// Gets the source identifier.
        /// </summary>
        /// <value>The source identifier.</value>
		public string SourceIdentifier
		{
			get {
                return "";
			}
		}

        /// <summary>
        /// Gets or sets the transport.
        /// </summary>
        /// <value>The transport.</value>
		public Transport Transport {
			get {
				return transport;
			}
			set {
				if (transport != null) {
					transport.DataReceived -= HandleTransportDataReceived;
				}
				transport = value;
				if (transport != null) {
					transport.DataReceived += HandleTransportDataReceived;
				}
				if (TransportChanged != null)
				{
					TransportChanged();
				}
			}
		}

        /// <summary>
        /// Reset this instance.
        /// </summary>
		public abstract void Reset();
		
		private void NotifyParsingData(Byte[] data)
		{
			if (ParsingData != null) {
				ParsingData(data);
			}
		}

        /// <summary>
        /// Notifies the license information received.
        /// </summary>
        /// <param name="modelNumber">Model number.</param>
        /// <param name="serialNumber">Serial number.</param>
		protected void NotifyLicenseInformationReceived(string modelNumber, string serialNumber)
		{
			if (LicenseInformationReceived != null)
			{
				LicenseInformationReceived(modelNumber, serialNumber);
			}
		}
	}
}
