using Internationalization;
using Logging;
using ProbeNet.Backend;
using ProbeNet.Messages.Raw;
/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Datacollector.Sources
{
    /// <summary>
    /// Class for loading backends.
    /// </summary>
    public class BackendLoader
    {
        private static readonly DeviceInformationComparer deviceInformationComparer =
             new DeviceInformationComparer();

        private I18n i18n;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Sources.BackendLoader"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        public BackendLoader(I18n i18n)
        {
            this.i18n = i18n;
        }

        /// <summary>
        /// Builds the handle with the specified serial number and device descripton from the device information having
        /// the specified identifier.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="identifier">Identifier.</param>
        /// <param name="serialNumber">Serial number.</param>
        /// <param name="backends">Backends.</param>
        /// <param name="deviceDescriptions">Device descriptions.</param>
        public Handle BuildHandle(string identifier, string serialNumber, IList<DeviceInformation> backends, IDictionary<string, DeviceDescription> deviceDescriptions)
        {
            foreach (DeviceInformation information in backends) {
                if (information.Identifier == identifier) {
                    return information.BuildHandle(serialNumber, deviceDescriptions);
                }
            }
            throw new ArgumentException(String.Format("No backend with identifier \"{0}\" found", identifier));
        }

        /// <summary>
        /// Load all backends from all available assemblies.
        /// </summary>
        /// <returns>The backends.</returns>
        public IList<DeviceInformation> LoadBackends()
        {
            List<DeviceInformation> backends = new List<DeviceInformation>();

            string[] files = Directory.GetFiles(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "probenet-backend-*.dll");
            foreach (string fileName in files) {
                try {
                    Assembly assembly = Assembly.LoadFile(fileName);
                    backends.AddRange(LoadBackendsFromAssembly(assembly));
                } catch (Exception e) {
                    Logger.Log(e, "Error while loading backend from file {0}", fileName);
                }
            }
            backends.Sort(deviceInformationComparer);
            return backends;
        }

        /// <summary>
        /// Loads the backends from specified assembly.
        /// </summary>
        /// <returns>The backends assembly to load from.</returns>
        /// <param name="assembly">Assembly.</param>
        public List<DeviceInformation> LoadBackendsFromAssembly(Assembly assembly)
        {
            List<DeviceInformation> backends = new List<DeviceInformation>();

            Type[] allTypes = assembly.GetExportedTypes();
            foreach (Type type in allTypes) {
                if (type.IsSubclassOf(typeof(EntryPoint))) {
                    EntryPoint entryPoint = Activator.CreateInstance(type, i18n) as EntryPoint;
                    backends.AddRange(entryPoint.LoadDeviceInformation());
                }
            }
            return backends;
        }
    }
}

