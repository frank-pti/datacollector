using Datacollector.Configuration;
using Datacollector.Transportation;
using Logging;
using ProbeNet.Backend;
using ProbeNet.Backend.Native;
using ProbeNet.Messages.Interface;
/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.IO;

namespace Datacollector.Sources
{
    /// <summary>
    /// The data source.
    /// </summary>
    public class DataSource<D>
        where D : DataSourceSettings
    {
        /// <summary>Delegate method for sequence series metadata received event.</summary>
        public delegate void SequenceSeriesMetadataReceivedCallback(ISequenceSeriesMetadata metadata);
        /// <summary>Delegate method for sequence received event.</summary>
        public delegate void SequenceReceivedCallback(ISequence sequence);
        /// <summary>Delegate method for active changed event.</summary>
        public delegate void ActiveStateChangedCallback(DataSource<D> dataSource, ConnectionState state);
        /// <summary>Delegate method for name changed event.</summary>
        public delegate void NameChangedCallback(DataSource<D> dataSource);
        /// <summary>Delegate method for transport changed event.</summary>
        public delegate void TransportChangedCallback(DataSource<D> dataSource);
        /// <summary>Delegate method for exception caught event.</summary>
        public delegate void ExceptionCaughtCallback(Exception exception);
        /// <summary>Delegate method for show parsing error event.</summary>
        public delegate void ShowParsingErrorCallback(string englishMessage, string localizedMessage);
        /// <summary>delegate method for transfer active event.</summary>
        public delegate void TransferActiveCallback(bool active);

        /// <summary>
        /// Occurs when sequence series metadata received.
        /// </summary>
        public event SequenceSeriesMetadataReceivedCallback SequenceSeriesMetadataReceived;
        /// <summary>
        /// Occurs when active state changed.
        /// </summary>
        public event ActiveStateChangedCallback ActiveStateChanged;
        /// <summary>
        /// Occurs when sequence received.
        /// </summary>
        public event SequenceReceivedCallback SequenceReceived;
        /// <summary>
        /// Occurs when name changed.
        /// </summary>
        public event NameChangedCallback NameChanged;
        /// <summary>
        /// Occurs when exception caught.
        /// </summary>
        public event ExceptionCaughtCallback ExceptionCaught;
        /// <summary>
        /// Occurs when transport changed.
        /// </summary>
        public event TransportChangedCallback TransportChanged;
        /// <summary>
        /// Occurs when show parsing error.
        /// </summary>
        public event ShowParsingErrorCallback ShowParsingError;
        /// <summary>
        /// Occurs when outgoing transfer active.
        /// </summary>
        public event TransferActiveCallback OutgoingTransferActive;
        /// <summary>
        /// Occurs when incoming transfer active.
        /// </summary>
        public event TransferActiveCallback IncomingTransferActive;

        private const string MagicSerialNumber = "66666";

        private D settings;
        private Transport transport;
        private Handle deviceHandle;
        private string dumpDirectory;
        private FileStream dumpStream;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Sources.DataSource`1"/> class.
        /// </summary>
        /// <param name="deviceHandle">Device handle.</param>
        /// <param name="transport">Transport.</param>
        /// <param name="name">Name.</param>
        /// <param name="dumpDirectory">Dump directory.</param>
        /// <param name="settings">Settings.</param>
        public DataSource(Handle deviceHandle, Transport transport, string name, string dumpDirectory, D settings)
        {
            Settings = settings;
            DeviceHandle = deviceHandle;
            Transport = transport;
            Name = name;
            this.dumpDirectory = dumpDirectory;
        }

        private void OnExceptionCaught(Exception e)
        {
            NotifyExceptionCaught(e);
            deviceHandle.Parser.Reset();
        }

        /// <summary>
        /// Gets the device handle.
        /// </summary>
        /// <value>The device handle.</value>
        public Handle DeviceHandle
        {
            get
            {
                return deviceHandle;
            }
            private set
            {
                if (deviceHandle != null) {
                    deviceHandle.Parser.SendSequenceSeriesMetadata -= HandleParserSendSequenceSeriesMetadata;
                    deviceHandle.Parser.SendSequence -= HandleParserSequenceReceived;
                    deviceHandle.Parser.ParsingError -= HandleParserError;
                    deviceHandle.Parser.SendDataToSource -= HandleParserSendDataToSource;
                }
                deviceHandle = value;
                if (deviceHandle != null) {
                    deviceHandle.Parser.SendSequenceSeriesMetadata += HandleParserSendSequenceSeriesMetadata;
                    deviceHandle.Parser.SendSequence += HandleParserSequenceReceived;
                    deviceHandle.Parser.ParsingError += HandleParserError;
                    deviceHandle.Parser.SendDataToSource += HandleParserSendDataToSource;
                }
            }
        }

        private void HandleParserSendDataToSource(Byte[] data, int offset, int count)
        {
            NotifyOutgoingTransferActive(true);
            if (Active) {
                Transport.SendData(data, offset, count);
            }
            NotifyOutgoingTransferActive(false);
        }

        private void HandleParserSendSequenceSeriesMetadata(ISequenceSeriesMetadata metadata)
        {
            NotifySequenceSeriesMetadataReceived(metadata);
        }

        private void NotifySequenceSeriesMetadataReceived(ISequenceSeriesMetadata metadata)
        {
            if (SequenceSeriesMetadataReceived != null) {
                SequenceSeriesMetadataReceived(metadata);
            }
        }

        /// <summary>
        /// Notifies the active state changed.
        /// </summary>
        /// <param name="state">State.</param>
        protected void NotifyActiveStateChanged(ConnectionState state)
        {
            if (ActiveStateChanged != null) {
                ActiveStateChanged(this, state);
            }
            if (state == Transportation.ConnectionState.Connected) {
                deviceHandle.UpdateDeviceDescription();
            }
        }

        private void NotifyTransportChanged()
        {
            if (TransportChanged != null) {
                TransportChanged(this);
            }
        }

        private void HandleParserError(bool showInteractive, Exception cause, string englishMessage, string localizedMessage)
        {
            Logger.Error("Parsing error occurred: {0}", englishMessage);
            if (cause != null) {
                Logger.Log(cause);
            }
            if (showInteractive) {
                NotifyShowParsingError(englishMessage, localizedMessage);
            }
        }

        private void NotifyShowParsingError(string englishMessage, string localizedMessage)
        {
            if (ShowParsingError != null) {
                ShowParsingError(englishMessage, localizedMessage);
            }
        }

        private void HandleParserSequenceReceived(ISequence sequence)
        {
            NotifySequenceReceived(sequence);
        }

        private void NotifySequenceReceived(ISequence sequence)
        {
            if (SequenceReceived != null) {
                SequenceReceived(sequence);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this data source is active.
        /// </summary>
        /// <value><c>true</c> if active; otherwise, <c>false</c>.</value>
        public bool Active
        {
            get
            {
                return transport != null && transport.Active;
            }
            set
            {
                deviceHandle.Parser.Reset();
                transport.Active = value;
            }
        }

        /// <summary>
        /// Gets or sets the connection state of the underlying transport
        /// </summary>
        /// <value>The state of the connection.</value>
        public ConnectionState ConnectionState
        {
            get
            {
                return transport.State;
            }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get
            {
                return settings.Name;
            }
            set
            {
                if (settings.Name != value) {
                    settings.Name = value;
                }
            }
        }

        private void NotifyNameChanged()
        {
            if (NameChanged != null) {
                NameChanged(this);
            }
        }

        private void HandleSettingsSerialNumberChanged()
        {
            if (deviceHandle != null) {
                deviceHandle.SerialNumber = settings.SerialNumber;
            }
        }

        /// <summary>
        /// Gets or sets the data source settings.
        /// </summary>
        /// <value>The settings.</value>
        public D Settings
        {
            get
            {
                return settings;
            }
            set
            {
                if (settings == value) {
                    return;
                }
                if (settings != null) {
                    settings.NameChanged -= NotifyNameChanged;
                    settings.SerialNumberChanged -= HandleSettingsSerialNumberChanged;
                }
                settings = value;
                if (settings != null) {
                    settings.NameChanged += NotifyNameChanged;
                    settings.SerialNumberChanged += HandleSettingsSerialNumberChanged;
                }
                NotifyNameChanged();
                HandleSettingsSerialNumberChanged();
            }
        }

        /// <summary>
        /// Gets or sets the transport.
        /// </summary>
        /// <value>The transport.</value>
        public Transport Transport
        {
            get
            {
                return transport;
            }
            set
            {
                if (transport != null) {
                    transport.ConnectionStateChanged -= HandleTransportConnectionStateChanged;
                    transport.ExceptionCaught -= OnExceptionCaught;
                    transport.DataReceived -= HandleTransportDataReceived;
                    if (transport.State != ConnectionState.Disconnected) {
                        throw new InvalidOperationException("Cannot exchange the transport while it is not disconnected");
                    }
                }
                transport = value;
                if (transport != null) {
                    transport.ConnectionStateChanged += HandleTransportConnectionStateChanged;
                    transport.ExceptionCaught += OnExceptionCaught;
                    transport.DataReceived += HandleTransportDataReceived;
                }
                NotifyTransportChanged();
            }
        }

        /// <summary>
        /// Handles the transport data received.
        /// </summary>
        /// <param name="data">Data.</param>
        public void HandleTransportDataReceived(byte[] data)
        {
            NotifyIncomingTransferActive(true);
            if (settings.DumpIncomingData) {
                DumpData(data);
            }
            deviceHandle.Parser.Parse(data, 0, data.Length);
            NotifyIncomingTransferActive(false);
        }

        private void HandleTransportConnectionStateChanged(ConnectionState state)
        {
            try {
                switch (state) {
                    case ConnectionState.Connected:
                        StartDumping();
                        break;
                    default:
                        StopDumping();
                        break;
                }
            } catch (Exception e) {
                Logger.Log(e);
            }
            NotifyActiveStateChanged(state);
        }

        private void StartDumping()
        {
            if (!settings.DumpIncomingData) {
                return;
            }
            if (!Directory.Exists(dumpDirectory)) {
                Directory.CreateDirectory(dumpDirectory);
            }
            string fileName = Path.Combine(dumpDirectory, BuildDumpFileName());
            StopDumping();
            dumpStream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read);
            if (dumpStream != null) {
                Logger.Log("Start dumping from source '{0}' ({1}) of type {2} to file '{3}'",
                    settings.Name, settings.SerialNumber, settings.ParserType, fileName);
            } else {
                Logger.Error("Could not start dumping source '{0}' ({1}) of type {2} to file '{2}'",
                    settings.Name, settings.SerialNumber, settings.ParserType, fileName);
            }
        }

        private string BuildDumpFileName()
        {
            return String.Format("{0} {1:yyyy-MM-dd-HH-mm-ss}.dump", settings.Name, DateTime.Now);
        }

        private void StopDumping()
        {
            if (dumpStream != null) {
                dumpStream.Flush();
                dumpStream.Close();
                dumpStream = null;
            }
        }

        private void DumpData(byte[] data)
        {
            try {
                if (dumpStream != null) {
                    dumpStream.Write(data, 0, data.Length);
                    dumpStream.Flush();
                }
            } catch (Exception e) {
                Logger.Log(e);
            }
        }

        /// <summary>
        /// Builds the configuration from transport.
        /// </summary>
        /// <returns>The configuration.</returns>
        public DataSourceConfiguration<D> BuildConfiguration()
        {
            return transport.BuildDatasourceConfiguration(settings);
        }

        private void NotifyExceptionCaught(Exception e)
        {
            if (ExceptionCaught != null) {
                ExceptionCaught(e);
            }
        }

        /// <summary>
        /// Determines whether this instance is valid license for the specified sourceType and serialNumber.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid license the specified sourceType serialNumber; otherwise, <c>false</c>.</returns>
        /// <param name="sourceType">Source type.</param>
        /// <param name="serialNumber">Serial number.</param>
        public bool IsValidLicense(string sourceType, string serialNumber)
        {
            if (!DeviceHandle.RequiresLicense) {
                return true;
            }
            if (sourceType != settings.ParserType) {
                Logger.Error("Source type: expected {0}, received {1}", settings.ParserType, sourceType);
                return false;
            }
            if (serialNumber != settings.SerialNumber) {
                Logger.Error("Serial number: expected {0}, received {1}", settings.SerialNumber, serialNumber);
                return false;
            }
            return true;
        }

        private void NotifyOutgoingTransferActive(bool active)
        {
            if (OutgoingTransferActive != null) {
                OutgoingTransferActive(active);
            }
        }

        private void NotifyIncomingTransferActive(bool active)
        {
            if (IncomingTransferActive != null) {
                IncomingTransferActive(active);
            }
        }
    }
}
