/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Backend;
using System.Collections.Generic;

namespace Datacollector.Sources
{
    /// <summary>
    /// Comparer class for device information.
    /// </summary>
    public class DeviceInformationComparer: IComparer<DeviceInformation>
    {
        private const string PTI = "PTI";
        private const string FRANK = "FRANK";

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Sources.DeviceInformationComparer"/> class.
        /// </summary>
        public DeviceInformationComparer ()
        {
        }

        #region IComparer[DeviceInformation] implementation
        /// <inheritdoc/>
        public int Compare (DeviceInformation x, DeviceInformation y)
        {
            String xCaption = x.Name.Tr;
            String yCaption = y.Name.Tr;
            if (xCaption.ToUpper().StartsWith(PTI)) {
                if (yCaption.ToUpper().StartsWith(PTI)) {
                    int comp = String.Compare(
                        xCaption.Substring(PTI.Length),
                        yCaption.Substring(PTI.Length),
                        StringComparison.OrdinalIgnoreCase);
                    return comp;
                }
                return -1;
            }
            if (xCaption.ToUpper().StartsWith(FRANK)) {
                if (yCaption.StartsWith(PTI)) {
                    return 1;
                }
                if (yCaption.ToUpper().StartsWith(FRANK)) {
                    int comp = String.Compare(
                        xCaption.Substring(FRANK.Length),
                        yCaption.Substring(FRANK.Length),
                        StringComparison.OrdinalIgnoreCase);
                    return comp;
                }
                return -1;
            }
            int compare = String.Compare(xCaption, yCaption, StringComparison.OrdinalIgnoreCase);
            return compare;
        }
        #endregion
    }
}

