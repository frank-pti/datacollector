/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Datacollector.Configuration;
using Internationalization;
using ProbeNet.Backend;
using Datacollector.Sources;
using Datacollector.Transportation;

namespace Datacollector
{
    /// <summary>
    /// Base class for data source factories.
    /// </summary>
    public abstract class DataSourceFactory<D>
        where D: DataSourceSettings
    {
        /// <summary>
        /// Builds the data source from i18n, handle, transport and directory for dump files.
        /// </summary>
        /// <returns>The data source.</returns>
        /// <param name="i18n">I18n.</param>
        /// <param name="handle">Handle.</param>
        /// <param name="transport">Transport.</param>
        /// <param name="dumpFileDirectory">Dump file directory.</param>
        public abstract DataSource<D> BuildDataSource(
            I18n i18n, Handle handle, Transport transport, string dumpFileDirectory);

        /// <summary>
        /// Builds the data source from handle, transport, directory for dump files and data source settings.
        /// </summary>
        /// <returns>The data source.</returns>
        /// <param name="handle">Handle.</param>
        /// <param name="transport">Transport.</param>
        /// <param name="dumpFileDirectory">Dump file directory.</param>
        /// <param name="settings">Settings.</param>
        public abstract DataSource<D> BuildDataSource(
            Handle handle, Transport transport, string dumpFileDirectory, D settings);

        /// <summary>
        /// Builds the data source from i18n, data source configuration and directory for dump files.
        /// </summary>
        /// <returns>The data source.</returns>
        /// <param name="i18n">I18n.</param>
        /// <param name="configuration">Configuration.</param>
        /// <param name="dumpFileDirectory">Dump file directory.</param>
        public abstract DataSource<D> BuildDataSource(
            I18n i18n, DataSourceConfiguration<D> configuration, string dumpFileDirectory);
    }
}

