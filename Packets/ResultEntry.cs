/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Datacollector
{
    /// <summary>
    /// Result entry.
    /// </summary>
	public class ResultEntry
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.ResultEntry"/> class.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="unit">Unit.</param>
        /// <param name="val">Value.</param>
		public ResultEntry(string name, string unit, double val)
		{
			Name = name;
			Unit = unit;
			Value = val;
		}

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
		public string Name {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the unit.
        /// </summary>
        /// <value>The unit.</value>
		public string Unit {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
		public double Value {
            get;
            set;
        }

        /// <inheritdoc/>
		public override string ToString ()
		{
			return String.Format ("[ResultEntry: Name={0}, Unit={1}, Value={2}]", Name, Unit, Value);
		}
	}
}

