/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using System.Timers;
using Datacollector.Configuration;
using ProbeNet.Backend;

namespace Datacollector.Transportation
{
    /// <summary>
    /// Data transport for example data. 
    /// </summary>
    [TranslatableCaption(Constants.DataCollectorDomain, "Example Data Transport")]
    [TransportTypeIdentifier("ExampleDataTransport")]
    public class ExampleDataTransport: Transport
    {
        private Timer timer;
        private int datagramsSent = 0;
        private Handle deviceHandle;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Transportation.ExampleDataTransport"/> class.
        /// </summary>
        /// <param name="deviceHandle">Device handle.</param>
        public ExampleDataTransport(Handle deviceHandle)
        {
            this.deviceHandle = deviceHandle;
            timer = new Timer();
            timer.AutoReset = false;
            timer.Interval = 1000;
            timer.Elapsed += HandleTimerElapsed;
        }

        private void HandleTimerElapsed (object sender, ElapsedEventArgs e)
        {
            if (datagramsSent < deviceHandle.ExampleDataCount) {
                NotifyDataReceived(deviceHandle.GetExampleData(datagramsSent));
                datagramsSent++;
                timer.Enabled = true;
            }
        }

        /// <inheritdoc/>
        protected override void Connect()
        {
            datagramsSent = 0;
            State = ConnectionState.Connected;
            timer.Enabled = true;
        }

        /// <inheritdoc/>
        protected override void Disconnect()
        {
            timer.Enabled = false;
            State = ConnectionState.Disconnected;
        }

        /// <inheritdoc/>
        public override void SendData(byte[] data, int offset, int count)
        {
        }

        /// <inheritdoc/>
        public override DataSourceConfiguration<D> BuildDatasourceConfiguration<D>(D settings)
        {
            GenericDataSourceConfiguration<TransportSettings, D> configuration =
                new GenericDataSourceConfiguration<TransportSettings, D>(
                    settings,
                    new ExampleDataTransportSettings());

            return configuration as DataSourceConfiguration<D>;
        }

        /// <inheritdoc/>
        public override bool MustDeleteDataBeforeUse {
            get {
                return true;
            }
        }
    }
}

