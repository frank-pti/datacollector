﻿/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Datacollector.Configuration;
using Internationalization;
using Logging;
using ProbeNet.Backend;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Datacollector.Transportation
{
    /// <summary>
    /// Transport for using UDP server connection.
    /// </summary>
    [TranslatableCaption(Constants.DataCollectorDomain, "UDP connection, server on this computer")]
    [TransportTypeIdentifier(UdpServerTransportSettings.TypeIdentifier)]
    public class UdpServerTransport : Transport
    {
        private Socket listenSocket;

        /// <summary>
        /// Initializes a new instance of the <see cref=" Datacollector.Transportation.UdpServerTransport"/> class.
        /// </summary>
        /// <param name="handle">Handle.</param>
        public UdpServerTransport(Handle handle)
        {
            Port = 11000;
        }

        /// <inheritdoc/>
        protected override void Connect()
        {
            State = ConnectionState.Connecting;
            if (listenSocket == null) {
                listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                listenSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                Start();
            }
            State = ConnectionState.Connected;
        }

        private void Start()
        {
            Thread thread = new Thread(StartServer) {
                Name = "UDP Server Transport",
                IsBackground = true
            };
            thread.Start();
        }

        private void StartServer()
        {
            EndPoint endPoint = new IPEndPoint(IPAddress.Any, Port);
            listenSocket.Bind(endPoint);
            while (true) {
                try {
                    if (listenSocket.Poll(-1, SelectMode.SelectRead)) {
                        if (Active) {
                            byte[] buffer = new byte[listenSocket.Available];
                            int bytesRead = listenSocket.ReceiveFrom(buffer, ref endPoint);
                            byte[] frame = new byte[bytesRead];
                            Array.Copy(buffer, frame, bytesRead);
                            NotifyDataReceived(buffer);
                        }
                    } else {
                        Thread.Sleep(10);
                    }
                } catch (SocketException se) {
                    NotifyExceptionCaught(se);
                    Thread.Sleep(10);
                } catch (ObjectDisposedException ode) {
                    NotifyExceptionCaught(ode);
                    break;
                } catch (Exception e) {
                    NotifyExceptionCaught(e);
                    Thread.Sleep(10);
                }
            }
        }

        private void OnDataReceived(IAsyncResult ar)
        {
            Socket socket = (Socket)ar.AsyncState;
            int count = 0;
            try {
                count = socket.EndReceive(ar);
                if (count > 0) {
                    byte[] buffer = new byte[count];
                    socket.Receive(buffer);
                    Console.WriteLine("Received data: '{0}'", System.Text.Encoding.UTF8.GetString(buffer));
                    NotifyDataReceived(buffer);
                } else {
                    Console.WriteLine("Received 0 bytes.");
                }
            } catch (SocketException e) {
                if (e.SocketErrorCode == SocketError.ConnectionReset) {
                    Logger.Error("UDP Server: remote end disconnected");
                }
            } catch (Exception e) {
                NotifyExceptionCaught(e);
            } finally {
                try {
                    byte[] buffer = new byte[2048];
                    EndPoint endPoint = socket.LocalEndPoint;
                    listenSocket.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref endPoint, OnDataReceived, socket);
                } catch (ObjectDisposedException) { }
            }
        }

        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        /// <value>The port.</value>
        public int Port
        {
            get;
            set;
        }

        /// <inheritdoc/>
        protected override void Disconnect()
        {
            State = ConnectionState.Disconnecting;
            State = ConnectionState.Disconnected;
        }

        /// <inheritdoc/>
        public override void SendData(byte[] data, int offset, int count)
        {
            Logger.Error("Cannot send data to UDP clients.");
        }

        /// <inheritdoc/>
        public override DataSourceConfiguration<D> BuildDatasourceConfiguration<D>(D settings)
        {
            GenericDataSourceConfiguration<UdpServerTransportSettings, D> configuration =
                new GenericDataSourceConfiguration<UdpServerTransportSettings, D>(
                    settings,
                    new UdpServerTransportSettings() {
                        Port = Port
                    });
            return configuration as DataSourceConfiguration<D>;
        }

        /// <inheritdoc/>
        public override bool MustDeleteDataBeforeUse
        {
            get
            {
                return false;
            }
        }
    }
}
