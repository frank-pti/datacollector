/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Datacollector.Transportation
{
    /// <summary>
    /// States of the connection.
    /// </summary>
	public enum ConnectionState
	{
        /// <summary>
        /// Connection to device is not active. No data will be received.
        /// </summary>
		Disconnected,
        /// <summary>
        /// Connection to device is being established.
        /// </summary>
		Connecting,
        /// <summary>
        /// Conneciton to device is active, data will be received from the device.
        /// </summary>
		Connected,
        /// <summary>
        /// Connection to device is being closed.
        /// </summary>
		Disconnecting
	}
}

