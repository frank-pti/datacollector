using Datacollector.Configuration;
using Logging;
using Newtonsoft.Json;
/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Text;

namespace Datacollector.Transportation
{
    /// <summary>
    /// Base class for transports.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public abstract class Transport
    {
        /// <summary>
        /// Deelegate method for connection state changed event.
        /// </summary>
        public delegate void ConnectionStateChangedDelegate(ConnectionState state);
        /// <summary>
        /// Occurs when connection state changed.
        /// </summary>
        public event ConnectionStateChangedDelegate ConnectionStateChanged;

        /// <summary>
        /// Delegate method for data received event
        /// </summary>
        public delegate void DataReceivedDelegate(byte[] data);
        /// <summary>
        /// Occurs when data received.
        /// </summary>
        public event DataReceivedDelegate DataReceived;

        /// <summary>
        /// Delegate method for exception caught event.
        /// </summary>
        public delegate void ExceptionCaughtCallback(Exception exception);
        /// <summary>
        /// Occurs when exception caught.
        /// </summary>
        public event ExceptionCaughtCallback ExceptionCaught;

        private ConnectionState state;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Transportation.Transport"/> class.
        /// </summary>
        public Transport()
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether this transport is active.
        /// </summary>
        /// <value><c>true</c> if active; otherwise, <c>false</c>.</value>
        public bool Active
        {
            get
            {
                return State == ConnectionState.Connecting || State == ConnectionState.Connected;
            }
            set
            {
                if (value) {
                    switch (State) {
                        case ConnectionState.Connected:
                            break;
                        case ConnectionState.Connecting:
                            break;
                        case ConnectionState.Disconnected:
                            Connect();
                            break;
                        case ConnectionState.Disconnecting:
                            throw new InvalidOperationException("Cannot connect the transport, because it is currently in the process of disconnecting");
                    }
                } else {
                    switch (State) {
                        case ConnectionState.Connected:
                            Disconnect();
                            break;
                        case ConnectionState.Connecting:
                            Disconnect();
                            break;
                        case ConnectionState.Disconnected:
                            break;
                        case ConnectionState.Disconnecting:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the connection state.
        /// </summary>
        /// <value>The state.</value>
        public ConnectionState State
        {
            get
            {
                return state;
            }
            protected set
            {
                state = value;
                NotifyConnectionStateChanged(state);
            }
        }

        /// <summary>
        /// Connect the transport to the device.
        /// </summary>
        protected abstract void Connect();
        /// <summary>
        /// Disconnect the transport to the device.
        /// </summary>
        protected abstract void Disconnect();

        private void NotifyConnectionStateChanged(ConnectionState state)
        {
            if (ConnectionStateChanged != null) {
                ConnectionStateChanged(state);
            }
        }

        /// <summary>
        /// Notifies the data received.
        /// </summary>
        /// <param name="data">Data.</param>
        protected void NotifyDataReceived(byte[] data)
        {
            if (data != null && data.Length != 0 && DataReceived != null) {
                DataReceived(data);
            }
        }

        /// <summary>
        /// Send data to the device.
        /// </summary>
        /// <param name="data">Data to send.</param>
        /// <param name="offset">Offset.</param>
        /// <param name="count">Count.</param>
        public abstract void SendData(byte[] data, int offset, int count);

        /// <summary>
        /// Sends the data to the device.
        /// </summary>
        /// <param name="data">Data to send.</param>
        public void SendData(byte[] data)
        {
            SendData(data, 0, data.Length);
        }

        /// <summary>
        /// Sends the string to the device and use specified encoding.
        /// </summary>
        /// <param name="data">Data to send.</param>
        /// <param name="encoding">Encoding.</param>
        public void SendString(string data, Encoding encoding)
        {
            SendData(encoding.GetBytes(data));
        }

        /// <summary>
        /// Builds the datasource configuration.
        /// </summary>
        /// <returns>The datasource configuration.</returns>
        /// <param name="settings">Datasource settings.</param>
        /// <typeparam name="D">The 1st type parameter.</typeparam>
        public abstract DataSourceConfiguration<D> BuildDatasourceConfiguration<D>(D settings)
            where D : DataSourceSettings;

        /// <summary>
        /// Notifies the exception caught.
        /// </summary>
        /// <param name="e">E.</param>
        protected void NotifyExceptionCaught(Exception e)
        {
            Logger.Error("Exception caught in transport: {0}{1}{2}",
                e.Message, Environment.NewLine, e.StackTrace);
            if (ExceptionCaught != null) {
                ExceptionCaught(e);
            }
        }

        /// <summary>
        /// Gets a value indicating whether all data must deleted before this data source can be used.
        /// before use.
        /// </summary>
        /// <value><c>true</c> if data must be deleted before using this transport; otherwise, <c>false</c>.</value>
        public abstract bool MustDeleteDataBeforeUse
        {
            get;
        }
    }
}

