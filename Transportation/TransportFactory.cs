using Internationalization;
using ProbeNet.Backend;
/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Datacollector.Transportation
{
    /// <summary>
    /// Factory class for transports.
    /// </summary>
    public static class TransportFactory
    {
        /// <summary>
        /// Builds the transport for identifier and handle.
        /// </summary>
        /// <returns>The transport.</returns>
        /// <param name="identifier">Identifier.</param>
        /// <param name="handle">Handle.</param>
        public static Transport BuildTransport(string identifier, Handle handle)
        {
            return BuildTransport(GetTransportTypeFromIdentifier(identifier), handle);
        }

        /// <summary>
        /// Builds the transport for type and handle.
        /// </summary>
        /// <returns>The transport.</returns>
        /// <param name="type">Type.</param>
        /// <param name="handle">Handle.</param>
        public static Transport BuildTransport(Type type, Handle handle)
        {
            CheckThatTypeIsTransport(type);
            return (Transport)Activator.CreateInstance(type, handle);
        }

        /// <summary>
        /// Gets the transport type identifiers.
        /// </summary>
        /// <returns>The transport type identifiers.</returns>
        public static List<string> GetTransportTypeIdentifiers()
        {
            List<string> identifiers = new List<string>();
            foreach (Type type in TransportTypes) {
                CheckThatTypeIsTransport(type);

                string identifier = GetIdentifierOfTransportType(type);
                if (!identifiers.Contains(identifier)) {
                    identifiers.Add(identifier);
                }
            }
            return identifiers;
        }

        /// <summary>
        /// Gets the transport type from identifier.
        /// </summary>
        /// <returns>The transport type.</returns>
        /// <param name="identifier">Identifier.</param>
        public static Type GetTransportTypeFromIdentifier(string identifier)
        {
            foreach (Type type in TransportTypes) {
                CheckThatTypeIsTransport(type);
                if (GetIdentifierOfTransportType(type) == identifier) {
                    return type;
                }
            }
            throw new ArgumentException("No transport found with identifier {0}", identifier);
        }

        /// <summary>
        /// Gets the transport types.
        /// </summary>
        /// <value>The transport types.</value>
        public static Type[] TransportTypes
        {
            get
            {
                return transportTypes;
            }
        }

        private static readonly Type[] transportTypes = {
			typeof(TcpClientTransport),
			typeof(TcpServerTransport),
			typeof(SerialTransport),
            typeof(UdpServerTransport),
            typeof(ExampleDataTransport)
		};

        private static void CheckThatTypeIsTransport(Type type)
        {
            if (!type.IsSubclassOf(typeof(Transport))) {
                throw new ArgumentException("The type must inherit from the Transport class");
            }
        }

        /// <summary>
        /// Gets the type of the identifier of transport.
        /// </summary>
        /// <returns>The identifier of transport type.</returns>
        /// <param name="type">Type.</param>
        public static string GetIdentifierOfTransportType(Type type)
        {
            object[] attributes = type.GetCustomAttributes(typeof(TransportTypeIdentifierAttribute), false);
            if (attributes.Length > 0) {
                TransportTypeIdentifierAttribute attribute = (TransportTypeIdentifierAttribute)(attributes[0]);
                return attribute.Identifier;
            }
            throw new ArgumentException("The type must have a TransportTypeIdentifier Attribute");
        }

        /// <summary>
        /// Transports the name of the type.
        /// </summary>
        /// <returns>The type name.</returns>
        /// <param name="i18n">I18n.</param>
        /// <param name="type">Type.</param>
        public static TranslationString TransportTypeName(I18n i18n, Type type)
        {
            CheckThatTypeIsTransport(type);

            object[] attributes = type.GetCustomAttributes(typeof(TranslatableCaptionAttribute), false);
            if (attributes.Length == 0) {
                throw new ArgumentException("The type must have a TranslatableCaption attribute");
            }

            TranslatableCaptionAttribute attribute = (TranslatableCaptionAttribute)(attributes[0]);
            return attribute.GetTranslation(i18n);
        }
    }
}

