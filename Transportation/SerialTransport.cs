using Datacollector.Configuration;
using Internationalization;
using Logging;
using Newtonsoft.Json;
using ProbeNet.Backend;
/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.IO.Ports;
using System.Threading;

namespace Datacollector.Transportation
{
    /// <summary>
    /// Transport for using the serial port.
    /// </summary>
    [TranslatableCaption(Constants.DataCollectorDomain, "Serial connection")]
    [TransportTypeIdentifier("SerialTransport")]
    public class SerialTransport : Transport
    {
        private const int BUFFER_SIZE = 1024;

        private SerialPort serialPort;
        private Thread receiveThread;
        private string receiveThreadLock = "locking";
        private byte[] buffer = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Transportation.SerialTransport"/> class.
        /// </summary>
        /// <param name="handle">Handle.</param>
        public SerialTransport(Handle handle)
        {
            serialPort = GetNewSerialPort();
        }

        private SerialPort GetNewSerialPort()
        {
            SerialPort serialPort = new SerialPort() {
                Handshake = Handshake.None
            };
            return serialPort;
        }

        private SerialPort GetNewSerialPort(string portName, int baudRate)
        {
            SerialPort serialPort = GetNewSerialPort();
            serialPort.PortName = portName;
            serialPort.BaudRate = baudRate;
            return serialPort;
        }

        /// <summary>
        /// Gets or sets the baud rate.
        /// </summary>
        /// <value>The baud rate.</value>
        [JsonProperty("baudRate")]
        public int BaudRate
        {
            get
            {
                return serialPort.BaudRate;
            }
            set
            {
                serialPort.BaudRate = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the port.
        /// </summary>
        /// <value>The name of the port.</value>
        [JsonProperty("portName")]
        public string PortName
        {
            get
            {
                return serialPort.PortName;
            }
            set
            {
                serialPort.PortName = value;
            }
        }

        /// <inheritdoc/>
        protected override void Connect()
        {
            try {
                if (!serialPort.IsOpen) {
                    serialPort.Open();
                    StartReceiving();
                    State = ConnectionState.Connected;
                    Logger.Log("connected on serial port {0} with speed {1}: {2}", serialPort.PortName, serialPort.BaudRate, serialPort.IsOpen);
                }
            } catch (Exception e) {
                NotifyExceptionCaught(e);
                State = ConnectionState.Disconnected;
            }
        }

        private void StartReceiving()
        {
            if (Helper.RunningOnMono) {
                StartReceivingOnMono();
            } else {
                StartReceivingOnMsDotNet();
            }
        }

        private void StartReceivingOnMono()
        {
            lock (receiveThreadLock) {
                receiveThread = new Thread(new ThreadStart(ReceiveData)) {
                    Name = "SerialTransportReceiverThread"
                };
                receiveThread.IsBackground = true;
                receiveThread.Start();
            }
        }

        private void StartReceivingOnMsDotNet()
        {
            buffer = new byte[BUFFER_SIZE];
            serialPort.BaseStream.BeginRead(buffer, 0, buffer.Length, DataReceivedAsync, null);
        }

        private void DataReceivedAsync(IAsyncResult result)
        {
            if (serialPort == null || !serialPort.IsOpen) {
                return;
            }
            try {
                int count = serialPort.BaseStream.EndRead(result);
                if (count > 0) {
                    byte[] copy = new byte[count];
                    Array.Copy(buffer, copy, copy.Length);
                    NotifyDataReceived(copy);
                }
                StartReceivingOnMsDotNet();
            } catch (Exception e) {
                Logger.Log(e, "Error while receiving data on serial transport");
            }
        }

        private void ReceiveData()
        {
            while (true) {
                try {
                    buffer = new byte[serialPort.BytesToRead];
                    int count = serialPort.Read(buffer, 0, buffer.Length);
                    byte[] newBuffer = new byte[count];
                    Array.Copy(buffer, newBuffer, count);
                    NotifyDataReceived(newBuffer);
                } catch (ThreadAbortException) {
                    // ignore thread abort, it was usually intended
                    return;
                } catch (System.IO.IOException) {
                    // this sometimes happens during shutdown
                    return;
                } catch (Exception e) {
                    NotifyExceptionCaught(e);
                    return;
                }
            }
        }

        /// <inheritdoc/>
        protected override void Disconnect()
        {
            try {
                if (serialPort.IsOpen) {
                    StopReceiving();
                    string portName = serialPort.PortName;
                    int baud = serialPort.BaudRate;

                    serialPort.Close();
                    serialPort.Dispose();
                    serialPort = GetNewSerialPort(portName, baud);
                    State = ConnectionState.Disconnected;
                }
            } catch (Exception e) {
                NotifyExceptionCaught(e);
            }
        }

        private void StopReceiving()
        {
            if (Helper.RunningOnMono) {
                StopReceivingOnMono();
            } else {
                StopReceivingOnMsDotNet();
            }
        }

        private void StopReceivingOnMono()
        {
            lock (receiveThreadLock) {
                if (receiveThread != null && receiveThread.IsAlive) {
                    receiveThread.Abort();
                    receiveThread = null;
                }
            }
        }

        private void StopReceivingOnMsDotNet()
        {
            // nothing to do here
        }

        private void Reconnect()
        {
            Disconnect();
            Connect();
        }

        /// <inheritdoc/>
        public override void SendData(byte[] data, int offset, int count)
        {
            if (serialPort.IsOpen) {
                serialPort.BaseStream.BeginWrite(data, offset, count, OnDataSent, null);
            }
        }

        private void OnDataSent(IAsyncResult result)
        {
            serialPort.BaseStream.EndWrite(result);
        }

        /// <inheritdoc/>
        public override DataSourceConfiguration<D> BuildDatasourceConfiguration<D>(D settings)
        {
            GenericDataSourceConfiguration<TransportSettings, D> configuration =
                new GenericDataSourceConfiguration<TransportSettings, D>(
                    settings,
                    new SerialTransportSettings() {
                        BaudRate = serialPort.BaudRate,
                        PortName = serialPort.PortName
                    });

            return configuration as DataSourceConfiguration<D>;
        }

        /// <inheritdoc/>
        public override bool MustDeleteDataBeforeUse
        {
            get
            {
                return false;
            }
        }
    }
}
