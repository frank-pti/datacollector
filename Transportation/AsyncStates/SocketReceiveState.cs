/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Net.Sockets;

namespace Datacollector.Transportation.AsyncStates
{
    /// <summary>
    /// Socket receive state.
    /// </summary>
	public class SocketReceiveState
	{
		private byte[] buffer;
		private Socket socket;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Transportation.AsyncStates.SocketReceiveState"/> class.
        /// </summary>
        /// <param name="socket">Socket.</param>
        /// <param name="bufferSize">Buffer size.</param>
		public SocketReceiveState(Socket socket, int bufferSize)
		{
			buffer = new byte[bufferSize];
			this.socket = socket;
		}

        /// <summary>
        /// Gets the buffer.
        /// </summary>
        /// <value>The buffer.</value>
		public byte[] Buffer {
			get {
				return buffer;
			}
		}

        /// <summary>
        /// Gets the socket.
        /// </summary>
        /// <value>The socket.</value>
		public Socket Socket {
			get {
				return socket;
			}
		}
	}
}

