/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Datacollector
{
    /// <summary>
    /// Attribute for the transport type identifier.
    /// </summary>
	public class TransportTypeIdentifierAttribute: Attribute
	{
		private string identifier;
		
        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.TransportTypeIdentifierAttribute"/> class.
        /// </summary>
        /// <param name="identifier">Identifier.</param>
		public TransportTypeIdentifierAttribute(string identifier)
		{
			this.identifier = identifier;
		}
		
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
		public string Identifier {
			get {
				return identifier;
			}
		}
	}
}

