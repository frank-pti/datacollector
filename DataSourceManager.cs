/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using Datacollector.Configuration;
using Internationalization;
using Logging;
using Datacollector.Sources;
using System.Linq;

namespace Datacollector
{
    /// <summary>
    /// Class for managing the data sources.
    /// </summary>
    public class DataSourceManager<D>
        where D : DataSourceSettings
    {
        private class FileNameComparer : IComparer<FileInfo>
        {
            public int Compare(FileInfo x, FileInfo y)
            {
                string xName = x.Name;
                string yName = y.Name;
                return string.Compare(xName, yName);
            }
        }

        private I18n i18n;
        private DirectoryInfo sourcesPath;

        private List<DataSource<D>> dataSources;
        private DataSourceFactory<D> dataSourceFactory;

        /// <summary>
        /// Delegate method for data source added event.
        /// </summary>
        public delegate void DataSourceAddedCallback(int index, DataSource<D> dataSource, bool interactive);
        /// <summary>
        /// Occurs when data source added.
        /// </summary>
        public event DataSourceAddedCallback DataSourceAdded;

        /// <summary>
        /// Delegate method for data source removed event.
        /// </summary>
        public delegate void DataSourceRemovedCallback(int index, DataSource<D> dataSource);
        /// <summary>
        /// Occurs when data source removed.
        /// </summary>
        public event DataSourceRemovedCallback DataSourceRemoved;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.DataSourceManager`1"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="sourcesPath">Path to the data sources configuration files.</param>
        /// <param name="dumpFileDirectory">Directory for dump files.</param>
        /// <param name="dataSourceFactory">Data source factory.</param>
        public DataSourceManager(
            I18n i18n, DirectoryInfo sourcesPath, string dumpFileDirectory, DataSourceFactory<D> dataSourceFactory)
        {
            this.i18n = i18n;
            this.sourcesPath = sourcesPath;
            this.dataSourceFactory = dataSourceFactory;
            string language = i18n.Language;
            string resourceName = String.Format("Datacollector.i18n.{0}.json", language);
            i18n.LoadFromResource(Constants.DataCollectorDomain, resourceName);
            i18n.LanguageChanged += HandleI18nLanguageChanged;
            dataSources = new List<DataSource<D>>();

            LoadConfiguration(dumpFileDirectory);

            BackendLoader backendLoader = new BackendLoader(i18n);
            Logger.Log("Loaded {0} backends", backendLoader.LoadBackends().Count);
        }

        private void LoadConfiguration(string dumpFileDirectory)
        {
            if (!sourcesPath.Exists) {
                sourcesPath.Create();
            }
            FileInfo[] configFileInfos = ListJsonFiles(sourcesPath);
            Array.Sort<FileInfo>(configFileInfos, new FileNameComparer());
            foreach (FileInfo configFile in configFileInfos) {
                DataSourceConfigurationHandler<D> loader = new DataSourceConfigurationHandler<D>(configFile.FullName);
                DummyDataSourceConfiguration<D> dummyConfiguration = loader.LoadDummyConfiguration();

                try {
                    DataSourceConfiguration<D> configuration;
                    switch (dummyConfiguration.TransportSettings.TransportType) {
                        case SerialTransportSettings.TypeIdentifier:
                            configuration = loader.LoadConfiguration<SerialTransportSettings>();
                            break;
                        case TcpClientTransportSettings.TypeIdentifier:
                            configuration = loader.LoadConfiguration<TcpClientTransportSettings>();
                            break;
                        case TcpServerTransportSettings.TypeIdentifier:
                            configuration = loader.LoadConfiguration<TcpServerTransportSettings>();
                            break;
                        case UdpServerTransportSettings.TypeIdentifier:
                            configuration = loader.LoadConfiguration<UdpServerTransportSettings>();
                            break;
                        case ExampleDataTransportSettings.TypeIdentifier:
                            configuration = loader.LoadConfiguration<ExampleDataTransportSettings>();
                            break;
                        default:
                            throw new ArgumentException(String.Format("Unknown transport type: {0}", dummyConfiguration.TransportSettings.TransportType));
                    }

                    DataSource<D> dataSource = dataSourceFactory.BuildDataSource(i18n, configuration, dumpFileDirectory);
                    dataSource.Name = configuration.DataSourceSettings.Name;
                    dataSources.Add(dataSource);
                } catch (InvalidCastException ice) {
                    Logger.Log(ice);
                }
            }
        }

        private FileInfo[] ListJsonFiles(DirectoryInfo directory)
        {
            return directory.GetFiles("*.json", SearchOption.TopDirectoryOnly);
        }

        private void HandleI18nLanguageChanged(I18n source, string language)
        {
            string resourceName = String.Format("Datacollector.i18n.{0}.json", language);
            source.LoadFromResource(Constants.DataCollectorDomain, resourceName);
        }

        /// <summary>
        /// Gets the data sources as read-only collection
        /// </summary>
        /// <value>The data sources.</value>
        public ReadOnlyCollection<DataSource<D>> DataSources
        {
            get
            {
                return dataSources.AsReadOnly();
            }
        }

        /// <summary>
        /// Gets a value indicating whether this data source manager has data sources.
        /// </summary>
        /// <value><c>true</c> if this instance has data sources; otherwise, <c>false</c>.</value>
        public bool HasDataSources
        {
            get
            {
                return dataSources.Count > 0;
            }
        }

        /// <summary>
        /// Adds the data source.
        /// </summary>
        /// <param name="dataSource">Data source.</param>
        /// <param name="interactive">If set to <c>true</c> interactive.</param>
        public void AddDataSource(DataSource<D> dataSource, bool interactive)
        {
            dataSources.Add(dataSource);
            if (DataSourceAdded != null) {
                DataSourceAdded(dataSources.Count - 1, dataSource, interactive);
            }
        }

        /// <summary>
        /// Removes the data source.
        /// </summary>
        /// <param name="dataSource">Data source.</param>
        public void RemoveDataSource(DataSource<D> dataSource)
        {
            if (!dataSources.Contains(dataSource)) {
                throw new ArgumentException("The data source is not inside the DataSourceManager");
            }
            int index = dataSources.IndexOf(dataSource);
            dataSource.Active = false;
            dataSources.Remove(dataSource);
            if (DataSourceRemoved != null) {
                DataSourceRemoved(index, dataSource);
            }
        }

        public IEnumerable<Guid> ConfiguredDataSources
        {
            get
            {
                return dataSources.Select(d => d.DeviceHandle.Description.Uuid);
            }
        }

        /// <summary>
        /// Gets the index of the specified data source.
        /// </summary>
        /// <returns>The index.</returns>
        /// <param name="dataSource">Data source.</param>
        public int GetIndex(DataSource<D> dataSource)
        {
            return dataSources.IndexOf(dataSource);
        }

        /// <summary>
        /// Shutdown the data sources.
        /// </summary>
        /// <param name="save">If set to <c>true</c> save.</param>
        public void Shutdown(bool save)
        {
            ShutdownDatasources();
            if (save) {
                SaveDatasources();
            }
        }

        private void ShutdownDatasources()
        {
            foreach (DataSource<D> source in dataSources) {
                source.Active = false;
            }
        }

        /// <summary>
        /// Saves the datasources.
        /// </summary>
        public void SaveDatasources()
        {
            List<string> savedFileNames = new List<string>();
            for (int i = 0; i < dataSources.Count; i++) {
                string fileName = Path.Combine(sourcesPath.FullName, String.Format("{0:0000}.json", i));
                FileInfo fileInfo = new FileInfo(fileName);
                SaveConfiguration(dataSources[i], fileName);
                savedFileNames.Add(fileInfo.FullName);
            }

            FileInfo[] existingFiles = ListJsonFiles(sourcesPath);
            foreach (FileInfo file in existingFiles) {
                if (!savedFileNames.Contains(file.FullName)) {
                    file.Delete();
                }
            }
        }

        private void SaveConfiguration(DataSource<D> dataSource, string fileName)
        {
            DataSourceConfigurationHandler<D> handler = new DataSourceConfigurationHandler<D>(fileName);
            handler.SaveDataSourceConfiguration(dataSource);
        }
    }
}
