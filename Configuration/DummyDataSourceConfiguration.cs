/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Newtonsoft.Json;

namespace Datacollector.Configuration
{
    /// <summary>
    /// Dummy data source configuration containing just a transport settings and data source settings.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class DummyDataSourceConfiguration<D>: DataSourceConfiguration<D>
        where D: DataSourceSettings
    {
        private TransportSettings transportSettings;
        private D dataSourceSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Configuration.DummyDataSourceConfiguration`1"/> class.
        /// </summary>
        public DummyDataSourceConfiguration ()
        {
        }

        /// <inheritdoc/>
        [JsonProperty("transportSettings")]
        public override TransportSettings TransportSettings {
            get {
                return transportSettings;
            }
            set {
                transportSettings = value;
            }
        }

        /// <inheritdoc/>
        [JsonProperty("dataSourceSettings")]
        public override D DataSourceSettings {
            get {
                return dataSourceSettings;
            }
            set {
                dataSourceSettings = value;
            }
        }
    }
}
