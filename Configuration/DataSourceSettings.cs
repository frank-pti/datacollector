/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Newtonsoft.Json;
using ProbeNet.Messages.Raw;
using System.Collections.Generic;

namespace Datacollector.Configuration
{
    /// <summary>
    /// Data source settings.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class DataSourceSettings
    {
        /// <summary>
        /// Delegate method for property changed event
        /// </summary>
        public delegate void PropertyChangedDelegate();
        /// <summary>
        /// Occurs when name changed.
        /// </summary>
        public event PropertyChangedDelegate NameChanged;
        /// <summary>
        /// Occurs when serial number changed.
        /// </summary>
        public event PropertyChangedDelegate SerialNumberChanged;

        private string name;
        private string serialNumber;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Configuration.DataSourceSettings"/> class.
        /// </summary>
        public DataSourceSettings()
        {
            DumpIncomingData = false;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [JsonProperty("name")]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name != value) {
                    name = value;
                    if (NameChanged != null) {
                        NameChanged();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        /// <value>The serial number.</value>
        [JsonProperty("serialNumber")]
        public string SerialNumber
        {
            get
            {
                return serialNumber;
            }
            set
            {
                if (serialNumber != value) {
                    serialNumber = value;
                    if (SerialNumberChanged != null) {
                        SerialNumberChanged();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of the parser.
        /// </summary>
        /// <value>The type of the parser.</value>
        [JsonProperty("parserType")]
        public string ParserType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the data source should
        /// dump incoming data.
        /// </summary>
        /// <value><c>true</c> if dump incoming data; otherwise, <c>false</c>.</value>
        [JsonProperty("dumpIncomingData")]
        public bool DumpIncomingData
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ProbeNet device information.
        /// </summary>
        /// <value>The probe net device information.</value>
        [JsonProperty("probeNetDeviceInformation")]
        public Dictionary<string, DeviceDescription> ProbeNetDeviceInformation
        {
            get;
            set;
        }
    }
}

