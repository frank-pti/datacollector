/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Newtonsoft.Json;

namespace Datacollector.Configuration
{
    /// <summary>
    /// Generic data source configuration.
    /// </summary>
	public class GenericDataSourceConfiguration<T, D>: DataSourceConfiguration<D>
		where T: TransportSettings
        where D: DataSourceSettings
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Configuration.GenericDataSourceConfiguration`2"/> class.
        /// </summary>
		public GenericDataSourceConfiguration()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Configuration.GenericDataSourceConfiguration`2"/> class.
        /// </summary>
        /// <param name="dataSourceSettings">Data source settings.</param>
        /// <param name="transportSettings">Transport settings.</param>
		public GenericDataSourceConfiguration(D dataSourceSettings, T transportSettings)
		{
			DataSourceSettings = dataSourceSettings;
			TransportSettings = transportSettings;
		}

        /// <summary>
        /// Gets or sets the generic transport settings.
        /// </summary>
        /// <value>The generic transport settings.</value>
		[JsonProperty ("transportSettings")]
        public T GenericTransportSettings {
            get;
            set;
        }

        /// <inheritdoc/>
		public override TransportSettings TransportSettings {
			get {
				return GenericTransportSettings;
			}
			set {
				GenericTransportSettings = value as T;
			}
		}

        /// <inheritdoc/>
		[JsonProperty ("dataSourceSettings")]
        public override D DataSourceSettings {
            get;
            set;
        }
	}
}

