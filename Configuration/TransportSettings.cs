/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Datacollector.Transportation;
using Newtonsoft.Json;
using ProbeNet.Backend;

namespace Datacollector.Configuration
{
    /// <summary>
    /// Base class for transport settings.
    /// </summary>
	[JsonObject(MemberSerialization.OptIn)]
	public class TransportSettings
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Configuration.TransportSettings"/> class.
        /// </summary>
		public TransportSettings()
		{
		}

        /// <summary>
        /// Gets or sets the type of the transport.
        /// </summary>
        /// <value>The type of the transport.</value>
		[JsonProperty ("transportType")]
        public string TransportType {
            get;
            set;
        }

        /// <summary>
        /// Builds the transport for the specified handle.
        /// </summary>
        /// <returns>The transport.</returns>
        /// <param name="handle">Handle.</param>
		public virtual Transport BuildTransport(Handle handle) {
			throw new NotSupportedException();
		}
	}
}
