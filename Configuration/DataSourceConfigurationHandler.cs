/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using Datacollector.Sources;
using Logging;

namespace Datacollector.Configuration
{
    /// <summary>
    /// Data source configuration handler.
    /// </summary>
    public class DataSourceConfigurationHandler<D>
        where D : DataSourceSettings
    {
        private string fileName;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Configuration.DataSourceConfigurationHandler`1"/> class.
        /// </summary>
        /// <param name="fileName">File name.</param>
        public DataSourceConfigurationHandler(string fileName)
        {
            this.fileName = fileName;
        }

        /// <summary>
        /// Loads the dummy configuration.
        /// </summary>
        /// <returns>The dummy configuration.</returns>
        public DummyDataSourceConfiguration<D> LoadDummyConfiguration()
        {
            return LoadDataSourceConfiguration(typeof(DummyDataSourceConfiguration<D>)) as DummyDataSourceConfiguration<D>;
        }

        /// <summary>
        /// Loads the configuration.
        /// </summary>
        /// <returns>The configuration.</returns>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public DataSourceConfiguration<D> LoadConfiguration<T>()
            where T : TransportSettings
        {
            return (DataSourceConfiguration<D>)LoadDataSourceConfiguration(typeof(GenericDataSourceConfiguration<T, D>));
        }

        /// <summary>
        /// Loads the data source configuration of the specified type given by its type name.
        /// </summary>
        /// <returns>The data source configuration.</returns>
        /// <param name="typeName">Type name.</param>
        public object LoadDataSourceConfiguration(string typeName)
        {
            return LoadDataSourceConfiguration(FindTypeForName(typeName));
        }

        /// <summary>
        /// Loads the data source configuration for the specified type.
        /// </summary>
        /// <returns>The data source configuration.</returns>
        /// <param name="dataSourceType">Data source type.</param>
        public object LoadDataSourceConfiguration(Type dataSourceType)
        {
            return LoadFromFile(fileName, dataSourceType);
        }

        /// <summary>
        /// Saves the data source configuration from the given data source.
        /// </summary>
        /// <param name="source">Source.</param>
        public void SaveDataSourceConfiguration(DataSource<D> source)
        {
            DataSourceConfiguration<D> configuration = source.BuildConfiguration();
            SaveToFile(configuration);
        }

        /// <summary>
        /// Loads data source configuration from specified file.
        /// </summary>
        /// <returns>The configuration.</returns>
        /// <param name="fileName">File name.</param>
        /// <typeparam name="T">The configuration type.</typeparam>
        public static T LoadFromFile<T>(string fileName)
        {
            return (T)LoadFromFile(fileName, typeof(T));
        }

        private static object LoadFromFile(string fileName, Type targetType)
        {
            return DeserializeFile(fileName, targetType);
        }

        /// <summary>
        /// Saves configuration to file.
        /// </summary>
        /// <param name="dataSourceConfiguration">Data source configuration.</param>
        public void SaveToFile(DataSourceConfiguration<D> dataSourceConfiguration)
        {
            SerializeFile(fileName, dataSourceConfiguration);
        }

        private void SerializeFile(string fileName, DataSourceConfiguration<D> config)
        {
            FileStream writeStream = new FileStream(fileName, FileMode.Create);
            TextWriter textWriter = new StreamWriter(writeStream);
            SerializeStream(textWriter, config);
            textWriter.Close();
        }

        private void SerializeStream(TextWriter textWriter, DataSourceConfiguration<D> config)
        {
            JsonTextWriter jsonWriter = new JsonTextWriter(textWriter);
            jsonWriter.Formatting = Formatting.Indented;

            JsonSerializer serializer = new JsonSerializer();
            try {
                serializer.Serialize(jsonWriter, config);
            } catch (Exception e) {
                throw e;
            }
        }

        private static T DeserializeFile<T>(string fileName)
        {
            return (T)DeserializeFile(fileName, typeof(T));
        }

        private static object DeserializeFile(string fileName, Type targetType)
        {
            FileInfo fileInfo = new FileInfo(fileName);
            if (fileInfo.Length == 0) {
                Logger.Warning("Found an empty source configuration file!");
                return Activator.CreateInstance(targetType);
            }

            FileStream readStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            TextReader textReader = new StreamReader(readStream);
            return DeserializeStream(textReader, targetType);
        }

        private static T DeserializeStream<T>(TextReader textReader)
        {
            return (T)DeserializeStream(textReader, typeof(T));
        }

        private static object DeserializeStream(TextReader textReader, Type targetType)
        {
            JsonSerializer serializer = new JsonSerializer();
            try {
                return serializer.Deserialize(textReader, targetType);
            } catch (Exception e) {
                throw e;
            } finally {
                textReader.Close();
            }
        }

        private Type FindTypeForName(string name)
        {
            if (name == null) {
                return null;
            }
            Assembly assembly = Assembly.GetAssembly(typeof(DataSource<D>));
            return assembly.GetType(name);
        }

        private bool CheckImplementedInterface(Type type, Type interFace)
        {
            foreach (Type iface in type.GetInterfaces()) {
                if (iface.FullName == interFace.FullName) {
                    return true;
                }
            }
            return false;
        }

    }
}