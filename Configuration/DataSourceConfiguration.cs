/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Newtonsoft.Json;

namespace Datacollector.Configuration
{
    /// <summary>
    /// Base class for data source configuration
    /// </summary>
	[JsonObject(MemberSerialization.OptIn)]
	public abstract class DataSourceConfiguration<T>
        where T: DataSourceSettings
	{
        /// <summary>
        /// Gets or sets the transport settings.
        /// </summary>
        /// <value>The transport settings.</value>
		public abstract TransportSettings TransportSettings
		{
			get;
			set;
		}

        /// <summary>
        /// Gets or sets the data source settings.
        /// </summary>
        /// <value>The data source settings.</value>
		public abstract T DataSourceSettings {
			get;
			set;
		}
	}
}

