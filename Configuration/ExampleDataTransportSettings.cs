/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Newtonsoft.Json;
using Datacollector.Transportation;
using ProbeNet.Backend;

namespace Datacollector.Configuration
{
    /// <summary>
    /// Transport settings for the example transport.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class ExampleDataTransportSettings: TransportSettings
    {
        /// <summary>
        /// The type identifier.
        /// </summary>
        public const string TypeIdentifier = "ExampleDataTransport";

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Configuration.ExampleDataTransportSettings"/> class.
        /// </summary>
        public ExampleDataTransportSettings ()
        {
            TransportType = TypeIdentifier;
        }

        /// <inheritdoc/>
        public override Transport BuildTransport(Handle handle) {
            return new ExampleDataTransport(handle);
        }
    }
}

