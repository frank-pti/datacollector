/*
 * The base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Datacollector
{
    /// <summary>
    /// Helper methods.
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Gets a value indicating whether this code is running on mono.
        /// </summary>
        /// <value><c>true</c> if running on mono; otherwise, <c>false</c>.</value>
        public static bool RunningOnMono {
            get {
                return Type.GetType ("Mono.Runtime") != null;
            }
        }
    }
}

